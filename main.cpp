#include<cstdint>
#include<random>
#include<vector>
#include<set>
#include<chrono>
#include<iostream>

int main(void)
{
    std::cout << "Generating random table" << std::endl;

    const int range(0x110000);
    const int seed(8675309);

    std::mt19937 g1 (seed);
    std::uniform_int_distribution<uint64_t> distribution(0, UINT64_MAX);

    std::vector<uint64_t> randHash(range);

    std::set<uint64_t> hashSet;
    std::vector<int> bitCount(64, 0);

    auto start = std::chrono::steady_clock::now();

    uint64_t minval(UINT64_MAX), maxval(0);

    uint64_t checksum1(0), checksum2(UINT64_MAX);

    for(int i(0); i < range; i++)
    {
        uint64_t val(distribution(g1));
        randHash[i] = val;
        hashSet.insert(val);

        minval = std::min(minval, val);
        maxval = std::max(maxval, val);
        checksum1 += val;
        checksum2 ^= val;

        int j(0);
        while(val)
        {
            bitCount[j++] += val & 1;
            val >>= 1;
        }
    }

    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff = end-start;
    std::cout << "Time to fill and iterate a vector of "
              << randHash.size()
              << " uint64_t : "
              << diff.count()
              << " seconds"
              << std::endl;

    std::cout << "Minimum value: 0x"
              << std::hex
              << minval
              << std::endl;

    std::cout << "Maximum value: 0x"
              << std::hex
              << maxval
              << std::endl;

    std::cout << "Unique values: 0x"
              << std::hex
              << hashSet.size()
              << std::endl;

    std::cout << "Duplicate values: 0x"
              << std::hex
              << randHash.size() - hashSet.size()
              << std::endl;

    std::cout << "Checksum values: 0x"
              << std::hex
              << checksum1
              << ", 0x"
              << checksum2
              << std::endl;

    std::cout << "Bit counts" << std::endl;
    for(int count: bitCount)
    {
        std::cout << std::hex << " 0x" << count << std::endl;
    }
    return 0;
}